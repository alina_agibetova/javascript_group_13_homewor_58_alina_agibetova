import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NewUserComponent } from './new-user/new-user.component';
import { UserItemComponent } from './user-item/user-item.component';
import { UsersComponent } from './users/users.component';
import { UserService } from './shared/user.service';
import { GroupComponent } from './group/group.component';
import { GroupItemComponent } from './group/group-item/group-item.component';
import { GroupService } from './shared/group.service';

@NgModule({
  declarations: [
    AppComponent,
    NewUserComponent,
    UserItemComponent,
    UsersComponent,
    GroupComponent,
    GroupItemComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [UserService, GroupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
