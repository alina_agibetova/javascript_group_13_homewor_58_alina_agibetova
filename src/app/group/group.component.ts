import { Component, OnInit } from '@angular/core';
import { Group } from '../shared/group.modal';
import { GroupService } from '../shared/group.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {
  groups!: Group[];

  constructor(private groupService: GroupService){}

  ngOnInit(): void {
    this.groups = this.groupService.getItems();
    this.groupService.groupItemsChange.subscribe((groups: Group[]) => {
      this.groups = groups;
    });
  }

}
