import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { User } from '../shared/user.modal';
import { CATEGORIES } from '../shared/categories';
import { UserService } from '../shared/user.service';
import { Group } from '../shared/group.modal';
import { GroupService } from '../shared/group.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent {
  @ViewChild('groupInput') groupInput!: ElementRef;
  categories = CATEGORIES;
  userName = '';
  userMail = '';
  userActive = true;
  category = '';


  constructor(
    private userService: UserService,
    private groupService: GroupService,
    ){}

  createUser(event:Event){
    event.preventDefault();
    const user = new User(this.userName, this.userMail, this.userActive, this.category);
    this.userService.addUser(user);
  }

  createGroup(){
    const groupName = this.groupInput.nativeElement.value;

    const userGroup = new Group(groupName);
    this.groupService.addUserToGroup(groupName);
  }

}
