import { EventEmitter } from '@angular/core';
import { Group } from './group.modal';
import { User } from './user.modal';


export class GroupService {
  groupItemsChange = new EventEmitter<Group[]>();
  private groups: Group[] = [];

  getItems(){
    return this.groups.slice();
  }

  addUserToGroup(user: User){
    const existingItem = this.groups.find(group => group.user === user);
    const newItem = new Group(user);
    this.groups.push(newItem);
    this.groupItemsChange.emit(this.groups);
  }
}
