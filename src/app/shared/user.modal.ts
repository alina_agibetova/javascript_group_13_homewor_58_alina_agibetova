export class User {
  constructor(
    public userName: string,
    public userMail: string,
    public userActive: boolean,
    public categories: string,
  ){}

}
