import { User } from './user.modal';
import { EventEmitter } from '@angular/core';

export class UserService {
  userChange = new EventEmitter<User[]>();
  private users: User[] = [
    new User('Alina', 'alina@mail.ru', true, 'admin'),
    new User('Ermek', 'ermek@mail.ru', true, 'editor'),
  ];

  getUsers(){
    return this.users.slice();
  }

  addUser(user: User){
    this.users.push(user);
    this.userChange.emit(this.users);
  }
}
