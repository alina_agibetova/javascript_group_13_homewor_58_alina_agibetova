import { Component, Input } from '@angular/core';
import { User } from '../shared/user.modal';
import { GroupService } from '../shared/group.service';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent {
  @Input() user!: User;

  // @Output() userClick = new EventEmitter<User>();

  constructor(private groupService: GroupService){}


  onClick(){
    this.groupService.addUserToGroup(this.user);
  }

}
