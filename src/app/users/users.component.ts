import { Component, Input, OnInit } from '@angular/core';
import { User } from '../shared/user.modal';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[] = [];

  constructor(private userService: UserService){}

  ngOnInit(): void{
    this.users = this.userService.getUsers();
    this.userService.userChange.subscribe((users: User[]) => {
      this.users = users;
    });
  }

}
